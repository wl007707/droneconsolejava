//required packages
package dronesimulation;

//creating the canvas
public class ConsoleCanvas {

	private int canvasX;
	private int canvasY;
	private char[][] canvas;
	
	ConsoleCanvas(int x, int y) {
		//Get canvas size
		canvasX = x;
		canvasY = y;
		canvas = new char[x][y];
		for (int a = 0; a < canvasX; a++) 
		{
			for (int b = 0; b < canvasY; b++) 
			{
				canvas[a][b] = ' ';
				if (a == 0) {
					//Top wall
					canvas[a][b] = '#';
				}
				if (b == 0) {
					//Left wall
					canvas[a][b] = '#';
				}
				if (b == y - 1) {
					//Right wall
					canvas[a][b] = '#';
				}
				if (a == x - 1) {
					//Bottom wall
					canvas[a][b] = '#';
				}

			}
		}

	}

	//Displaying the canvas
	public String toString() {
		String res = "";
		for (int i = 0; i < canvasX; i++) {
			for (int j = 0; j < canvasY; j++) {
				res = res + canvas[i][j] + " ";
			}
			res = res + "\n";
		}
		return res;
	}
	
	//displaying drones on the canvas
	public void showIt(int droneX, int droneY, char ch) {
		canvas[droneX + 1][droneY + 1] = ch;
	}
	
	public static void main(String[] args) {
		// create a canvas
		ConsoleCanvas c = new ConsoleCanvas (10, 5);
		// add a Drone at 4,3
		c.showIt(4,3,'D');	
		// display result
		System.out.println(c.toString());			
	}

}
