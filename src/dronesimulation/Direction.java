//required packages
package dronesimulation;
import java.util.Random;


//direction class
public enum Direction {
	NORTH, SOUTH, EAST, WEST;
	
	//pick a new random direction
	public static Direction newRandomDir() {
		Random random = new Random();
		//chooses from list of direction
		return values()[random.nextInt(values().length)];	
	}
	
	//
	public Direction nextDirection() {
		//returns ordinal enumeration constant (the position in the enum declaration)
		return values()[(this.ordinal() + 1) % values().length];
	}
	
}
