//required packages
package dronesimulation;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.lang.Integer;

//interface class
public class DroneInterface {
	
 private Scanner s; // scanner used for input from user	
 private int x,y; 
 private String saveName;
 private DroneArena myArena;				// arena in which drones are shown
 String direc;
 
    /**
    	 * constructor for DroneInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     */
    public DroneInterface() {
    	 s = new Scanner(System.in);			// set up scanner for user input
    	 myArena = new DroneArena(0, 0);	// create a blank arena
    	 File location; //save directory location
    	 
    	 
    	//switch case for operation selection
        char ch = ' ';
        do {
        	System.out.print("\n(A)dd drone \n(I)nformation \n(D)isplay arena \n(M)ove drones \n(N)ew empty arena \n(P)rogress 10 movements \n(S)ave arena \n(L)oad arena\ne(X)it \nSelect operation from list above: ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        					myArena.addDrone();	// add a new drone to arena
        					break;
        					
    			case 'D' :
				case 'd' :
						System.out.println(myArena.toString());
						doDisplay(); //display current arena
						break;
						
				case 'M' :
				case 'm' :
						myArena.moveAllDrones(); //move all drones
						System.out.println(myArena.toString());
						doDisplay(); //display new arena state
						break;
						
				case 'N' :
				case 'n' :
					//create a new arena of custom values
					System.out.print("Enter arena width: ");
					x = s.nextInt();
					s.nextLine();
					System.out.print("Enter arena length: ");
					y = s.nextInt();
					s.nextLine();
					createArena(x,y);
					break;

				case 'P' :
				case 'p' :
					//play through 10 steps of motion
					for(int i = 0; i < 10; i++)
					{
						try {
							myArena.moveAllDrones();
							System.out.println(myArena.toString());
							doDisplay();
							Thread.sleep(200);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					break;


        		case 'I' :
        		case 'i' :
        			//get information on current drones that occupy arena
        					System.out.print(myArena.toString());
            				break;
        		case 'S' :
				case 's' :
					//save current arena
					location = new File("C:/Users/Ryan-/eclipse-workspace/dronesimulation/saves");
					System.out.print("Enter name for save: ");
					saveName = s.nextLine();

					System.out.print(saveName + " has been saved to your saves folder\n");
					
					//create new txt file
					File saveFile = new File("C:/Users/Ryan-/eclipse-workspace/dronesimulation/saves/" + saveName + ".txt");
					try {
						//write to new file
						FileWriter saveFileWriter = new FileWriter (saveFile);
						PrintWriter writeHead = new PrintWriter(saveFileWriter);
						
						//write x size of arena
						writeHead.println(myArena.getX());
						//write y size of arena
						writeHead.println(myArena.getY());
						
						//loop through for each drone
						for(Drone d : myArena.getDrones()) {
							//write drone id
							writeHead.println(d.getID());
							//drone x value
							writeHead.println(d.getX());
							//drone y value
							writeHead.println(d.getY());
							//and direction drone is facing
							writeHead.println(d.getDir());
						}
						//close the file
						writeHead.close();
					} 
					//error catching
					catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//Load a saved file
					break;
				case 'L' :
				case 'l' :
					System.out.print("Enter name of save to load: ");
					saveName = s.nextLine();
					
					String file = saveName + ".txt";
					System.out.print("Loading save " + saveName);
					
					//try to load
					try {
						int droneX, droneY, id;
						//retrieve file
						Path filePath = Paths.get("C:/Users/Ryan-/eclipse-workspace/dronesimulation/saves/" + file);
						System.out.println(filePath.toAbsolutePath());
						//get number of lines in file
						long lines = Files.lines(filePath).count();
						
						//Get first 2 lines for arena size
						int width = Integer.parseInt(Files.readAllLines(filePath).get(0),15);
						int length = Integer.parseInt(Files.readAllLines(filePath).get(1),15);
						createArena(width, length);
						
						//get info for a drone
						for(int i = 2; i < lines; i=i+4) {
							id = Integer.parseInt(Files.readAllLines(filePath).get(i),15);
					        droneX = Integer.parseInt(Files.readAllLines(filePath).get(i+1),15);
					        droneY = Integer.parseInt(Files.readAllLines(filePath).get(i+2),15);
					        direc = Files.readAllLines(filePath).get(i+3);
					        
					        //add the drone, then repeat for remaining drones
					        myArena.loadDrone(id, droneX, droneY, direc);
						}
						//display the loaded save
						doDisplay();
						System.out.println("Successfully loaded save " + saveName);
					}
					//error catching
					catch(IOException e){
						e.printStackTrace();
					}
					break;
					
        		case 'x' : 	ch = 'X';				// when X detected program ends
        							break;
        	}
    		} while (ch != 'X');						// test if end
        
       s.close();									// close scanner
    }
    
    //function to creat arena
    void createArena(int x, int y)
	{
    	//clear arena so no drones are out of range
    	myArena.clearArena();
    	myArena.setX(x);
		myArena.setY(y);
		doDisplay();
		System.out.println("Successfully created new empty arena");
	}

    //start the interface
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		DroneInterface r = new DroneInterface();	// just call the interface
	}
	
	//display the arena current state
	void doDisplay() {
			// determine the arena size
		int sizex = myArena.getX();
		int sizey = myArena.getY();
		//hence create a suitable sized ConsoleCanvas object
		ConsoleCanvas canvas = new ConsoleCanvas(sizex+2, sizey+2);//+2 makes the barriers in an unreachable part of canvas for drones
		//call showDrones suitably
		myArena.showDrones(canvas);
		//then use the ConsoleCanvas.toString method 
		System.out.println(canvas.toString());	
	}

}



