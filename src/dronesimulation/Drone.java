package dronesimulation;

//drone class
public class Drone {
	//setting up variables
	private int droneX;
	private int droneY;
	private int id;
	private static int nextAvailableId = 0;
	private Direction dir;
	
	//Drone object
	Drone(int x, int y, Direction d) 
	{
		droneX = x;
		droneY = y;
		id = nextAvailableId;
		nextAvailableId = nextAvailableId + 1;
		dir = d;
	}

	//getters
	public int getX() {
		return droneX;
	}

	public int getY() {
		return droneY;
	}
	
	public Direction getDir() {
		return dir;
	}
	
	public int getID() {
		return id;
	}

	//setters
	public void setID(int id) {
		this.id = id;
	}


	//return location
	public String toString() {
		return "Drone " + id + " is at " + droneX + "," + droneY + " facing " + dir.toString();
	}
	
	//Check if there is a drone in a location
	public boolean isHere(int x, int y) {
		if (x == droneX && y == droneY)
			return true;
		else
			return false;
	}
	
	//Draws drones to the canvas
	public void displayDrone(ConsoleCanvas c) 
	{
		c.showIt(droneX, droneY, 'D');
	}
	
	//Tries to move drones
	public void tryToMove(DroneArena a) {
		switch(dir) {
		case NORTH:
			if(a.canMoveHere(droneX - 1, droneY)){
				dir = dir.newRandomDir();
				droneX = droneX - 1;
			}
			else {
				dir = dir.nextDirection();
			}
			break;
		case SOUTH:
			if(a.canMoveHere(droneX + 1, droneY)){
				dir = dir.newRandomDir();
				droneX = droneX + 1;
			}
			else {
				dir = dir.nextDirection();
			}
			break;
		case EAST:
			if(a.canMoveHere(droneX, droneY + 1)){
				dir = dir.newRandomDir();
				droneY = droneY + 1;
			}
			else {
				dir = dir.nextDirection();
			}
			break;
		case WEST:
			if(a.canMoveHere(droneX, droneY - 1)){
				dir = dir.newRandomDir();
				droneY = droneY - 1;
			}
			else {
				dir = dir.nextDirection();
			}
			break;
		}
	}

	
//	public static void main(String[] args) {
//		Drone d = new Drone(5, 3);		// create drone
//		System.out.println(d.toString());	// print where is
//	}
	
	
}
