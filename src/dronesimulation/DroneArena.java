//Required packages for program to run
package dronesimulation;
import java.util.Random;
import java.util.ArrayList;

//Drone arena class
public class DroneArena {
	//Setting up variables
	private int arenaX;
	private int arenaY;
	private ArrayList<Drone> droneList = new ArrayList<Drone>();
	Drone newDrone;
	Drone loadedDrone;

	//Arena size
	public DroneArena(int x, int y)
	{
		arenaX = x;
		arenaY = y;
	}

	//add new drone function
	public void addDrone()
	{	
		int positionX,positionY;
		
		//Initialise random number
		Random randomGenerator;
		randomGenerator = new Random();
		//If the list of drones is less than the area, another drone can be added
		if (droneList.size() < arenaX * arenaY) {
			//If there is no drone, add it to the list at the position
			do {
				//Make sure to not add drone on arena boundary
				positionX = randomGenerator.nextInt(arenaX);
				positionY = randomGenerator.nextInt(arenaY);
			} while (getDroneAt(positionX, positionY) != null);
			//Add drone to the list
			newDrone = new Drone(positionX,positionY,Direction.newRandomDir());
			droneList.add(newDrone);
			
		}
		//if arena is full
		else
		{
			//error message
			System.err.print("Arena full. Too many drones");
		}
	}
	
	//load drone function
	public void loadDrone(int id, int x, int y, String direc)
	{
		//It checks what the saved direction was, then set the direction of the drone
		if(direc.equals("NORTH"))
		{
			 
			 loadedDrone = new Drone(x,y,Direction.NORTH);
		}
		if(direc.equals("EAST"))
		{
			
			loadedDrone = new Drone(x,y,Direction.EAST);
		}
		if(direc.equals("SOUTH"))
		{
		     
			loadedDrone = new Drone(x,y,Direction.SOUTH);
		}
		if(direc.equals("WEST"))
		{
			 
			loadedDrone = new Drone(x,y,Direction.WEST);

		}
		//sets the drone ID then adds it to list.
		loadedDrone.setID(id);
		droneList.add(loadedDrone);
	}

	
	//getter functions for arena
	public int getX() {
		return arenaX;
	}

	public int getY() {
		return arenaY;
	}
	
	public ArrayList<Drone> getDrones() {
		return droneList;
	}

	
	//setter functions for arena
	public void setX(int specifiedX) {
		this.arenaX = specifiedX;
	}

	public void setY(int specifiedY) {
		this.arenaY = specifiedY;
	}

	//Function to clear the drones when changing arena size
	public void clearArena() {
		droneList.clear();
	}
	
	//Checks for a drone at a loction
	public Drone getDroneAt(int x, int y) {
		//creates empty drone at location null
		Drone drone = null;
		for (Drone a : droneList) {
			if (a.isHere(x, y) == true) {
				//replaces temporary drone with the one you want
				drone = a;
			}
		}
		return drone;
	}
	
	//Checks if the drone can move to the space it wants
	public boolean canMoveHere(int x, int y)
	{
		//drone is not null, or chooses and area outside the arena, the drone wont be allowed to move there
		if(getDroneAt(x, y) != null || x >= arenaX || y >= arenaY || x < 0 || y < 0){
			return false;
		}
		else
		{
			return true;
		}
	}

	
	//show drones in arena
	public void showDrones(ConsoleCanvas c)
	{
		//loop for each drone in drone list
		for(Drone d : droneList)
		{
			d.displayDrone(c);
		}
	}

	//Function to progress the drones forward 1 step
	public void moveAllDrones() {
		//loop through every drone on the list and moves them (or at least tries to)
		for(Drone d : droneList) {
			d.tryToMove(this);
		}
	}
	
	//This gets the information about the arena's current state
	public String toString() {
		String str = "";
		//If the arena is made, and drones are in it
		if (arenaX > 0 && arenaY > 0 && droneList.isEmpty() == false) { 
			//gives the arena size and location of each drone
			str += "Arena size:" + arenaX + "x" + arenaY + " and ";
			for (int i = 0; i < droneList.size(); i++) {
				str += "\n" + droneList.get(i).toString();
			}
		} 
		else if (arenaX > 0 && arenaY > 0 && droneList.isEmpty() == true) {
			//If the arena is made, but there are no drones
			str += "Arena size is: " + arenaX + "x" + arenaY;
			System.err.print("\nThere are no drones in the arena\n");
		} 
		else {
			//If arena does not exist, and no drones are made
			System.err.print("\nArena not created, No drones exist either\n");
			str += ""; 

		}
		return str;
	}


}
